package com.android.ci.test.app.test;

import android.test.InstrumentationTestRunner;
import android.test.InstrumentationTestSuite;

import com.android.ci.test.app.LaunchActivityTest;
import com.android.ci.test.app.SQLiteTest;

import junit.framework.TestSuite;

/**
 * Created by javierayuso on 26/11/13.
 */
public class Runner extends InstrumentationTestRunner {

    @Override
    public TestSuite getAllTests(){
        InstrumentationTestSuite suite = new InstrumentationTestSuite(this);

        suite.addTestSuite(LaunchActivityTest.class);
        suite.addTestSuite(SQLiteTest.class);
        return suite;
    }

    @Override
    public ClassLoader getLoader() {
        return Runner.class.getClassLoader();
    }
}