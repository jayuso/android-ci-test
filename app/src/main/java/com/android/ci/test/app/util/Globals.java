package com.android.ci.test.app.util;

/**
 * Created by javierayuso on 21/12/13.
 */
public class Globals {

    public static final String LOG_TAG_UNIT_TEST = "UNIT_TEST";

    public static final String EXTRA_DATA_ID = "DataId";
}
