package com.android.ci.test.app.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.ci.test.app.data.Data;
import com.android.ci.test.app.util.Globals;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DataDAO {

    public static final String TABLE_DATA = "data";

    // Table field constants
    public static final String KEY_DATA_ID = "_id";
    public static final String KEY_DATA_NAME = "name";
    public static final String KEY_DATA_DESCRIPTION = "description";

    private static final String[] tableDataColumns = {
            KEY_DATA_ID,
            KEY_DATA_NAME,
            KEY_DATA_DESCRIPTION
    };

    private Context context;

    public DataDAO(Context context) {
        this.context = context;
    }

    /**
     *
     * @return a List containing all Servers from DB
     */
    public List<Data> query() {
        List<Data> l = null;

        Cursor c = this.queryCursor();
        if (c == null) {
            return null;
        }

        // creating a List based on Cursor size example
        if (c.getCount() == 0) {
            return null;
        } else if (c.getCount() < 100) {
            l = new ArrayList<Data>();
        } else {
            l = new LinkedList<Data>();
        }

        if (c.moveToFirst()) {
            do {
                Data t = dataFromCursor(c);

                l.add(t);

            } while (c.moveToNext());
        }
        c.close();
        return l;
    }

    // convenience method
    public static Data dataFromCursor(Cursor c) {
        assert c != null;

        Data t = new Data();
        t.setId(c.getLong(c.getColumnIndex(KEY_DATA_ID)));
        t.setName(c.getString(c.getColumnIndex(KEY_DATA_NAME)));
        t.setDescription(c.getString(c.getColumnIndex(KEY_DATA_DESCRIPTION)));

        return t;
    }

    public Cursor queryCursor() {
        // select
        DBHelper db = new DBHelper(context);

        Cursor c = db.getReadableDatabase().query(TABLE_DATA, tableDataColumns, null, null, null, null, KEY_DATA_NAME + " ASC");

        return c;
    }


    /**
     * Returns a Server object from its id
     * @param id - the server id in db
     * @return Server object if found, null otherwise
     */
    public Data query(long id) {
        if (id <= 0) {
            return null;
        }

        Data data = null;

        DBHelper db = new DBHelper(context);

        String where = KEY_DATA_ID + "=" + id;
        Cursor c = db.getReadableDatabase().query(TABLE_DATA,
                tableDataColumns,
                where,
                null,
                null,
                null,
                null);
        if (c != null) {
            if (c.getCount() > 0) {
                c.moveToFirst();
                data = dataFromCursor(c);
            }
        }
        c.close();
        db.close();
        return data;
    }


    private ContentValues getContentValues(Data data) {
        ContentValues content = new ContentValues();
        content.put(KEY_DATA_NAME, data.getName());
        content.put(KEY_DATA_DESCRIPTION, data.getDescription());

        return content;
    }

    public long insert(Data data) {
        if (data == null) {
            return 0;
        }

        long id = -1;
        try {
            // insert
            DBHelper db = new DBHelper(context);

            id = db.getWritableDatabase().insert(TABLE_DATA, null, this.getContentValues(data));

            db.close();
            db=null;
        } catch (Exception e) {
            Log.e(Globals.LOG_TAG_UNIT_TEST,e.getMessage());
        }

        return id;
    }

    public void update(long id, Data data) {
        if (data == null) {
            return;
        }

        DBHelper db = new DBHelper(context);

        db.getWritableDatabase().update(TABLE_DATA, this.getContentValues(data), KEY_DATA_ID + "=" + id, null);

        db.close();
        db=null;
    }

    public void delete(long id) {
        DBHelper db = new DBHelper(context);

        db.getWritableDatabase().delete(TABLE_DATA,  KEY_DATA_ID + " = " + id, null);

        db.close();
        db=null;
    }

    public void deleteAll() {
        DBHelper db = new DBHelper(context);

        db.getWritableDatabase().delete(TABLE_DATA,  null, null);

        db.close();
        db=null;
    }


    class DBHelper extends SQLiteOpenHelper {

        public static final String DATABASE_NAME = "data_db.sqlite";
        public static final int DATABASE_VERSION = 1;

        public static final String CREATE_DATA_TABLE =
                "create table "
                        + TABLE_DATA + "( " + KEY_DATA_ID
                        + " integer primary key autoincrement, "
                        + KEY_DATA_NAME + " text not null,"
                        + KEY_DATA_DESCRIPTION + " text "
                        + ");";

        public static final String CREATE_DATABASE = CREATE_DATA_TABLE;

        // TODO finish
        public static final String UPDATE_DB_FROM_V1_TO_V2 = "alter table " + TABLE_DATA;

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DATABASE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            if (oldVersion == 1 && newVersion == 2) {
                Log.i("ServerTracker", "Migrating from V1 to V2");
                // db.execSQL(UPDATE_DB_FROM_V1_TO_V2);
            }
            db.execSQL("DROP TABLE " + TABLE_DATA + ";");
            db.execSQL(CREATE_DATABASE);

        }

    }
}
