package com.android.ci.test.app.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by javierayuso on 27/12/13.
 */
public class Util {

    public static void goToGitHub(Context context) {
        Uri uriUrl = Uri.parse("http://github.com/jfeinstein10/slidingmenu");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        context.startActivity(launchBrowser);
    }
}
