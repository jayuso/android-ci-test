package com.android.ci.test.app;

import android.app.Application;

import com.testflightapp.lib.TestFlight;

/**
 * Created by javierayuso on 26/12/13.
 */
public class app extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        TestFlight.takeOff(this, "65bedebb-77b6-43d1-a42d-744ace0b07a6");
    }

}
