package com.android.ci.test.app;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.widget.EditText;

import com.android.ci.test.app.dao.DataDAO;
import com.android.ci.test.app.data.Data;
import com.android.ci.test.app.util.Globals;
import com.jayway.android.robotium.solo.Solo;

/**
 * Created by javierayuso on 25/11/13.
 */
public class SQLiteTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Solo solo;

    public SQLiteTest() {
        super(MainActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();

        solo = new Solo(getInstrumentation(),getActivity());
    }

    @MediumTest
    public void testDataCRUD() {

        Log.d(Globals.LOG_TAG_UNIT_TEST,"Initializing db");
        DataDAO dataDAO = new DataDAO(this.getActivity().getBaseContext());

        //Create
        Data data = new Data("name","description");
        long id = dataDAO.insert(data);
        assertTrue("Error inserting. Id lower than 1 ("+id+")",(id>0));

        //Read
        Data data2 = dataDAO.query(id);
        assertNotNull("Data recovered after insert is NULL",data2);
        assertEquals("ID is not equal", id, data2.getId());
        assertEquals("Name is not equal", data.getName(), data2.getName());
        assertEquals("Description is not equal",data.getDescription(),data2.getDescription());

        //Update
        data2.setName("new name");
        data2.setDescription("new description");
        dataDAO.update(id,data2);

        Data data3 = dataDAO.query(id);
        assertNotNull("Data recovered after insert is NULL", data3);
        assertEquals("Name is not equal",data2.getName(), data3.getName());
        assertEquals("Description is not equal",data2.getDescription(),data3.getDescription());

        //Delete
        dataDAO.delete(id);
        Data data4 = dataDAO.query(id);
        assertNull(data4);
    }

    @MediumTest
    public void testLoadList() {

        solo.clickOnView(solo.getView(R.id.main_button_sqlite_list));

        solo.waitForActivity(SQLiteListActivity.class.getName(),25);
        solo.assertCurrentActivity("Failure to start SQLiteListActivity",SQLiteListActivity.class);

        solo.finishOpenedActivities();
    }

    @MediumTest
    public void testAddEdit() {

        solo.clickOnView(solo.getView(R.id.main_button_sqlite_add));

        solo.waitForActivity(SQLiteEditActivity.class.getName(),25);
        solo.assertCurrentActivity("Failure to start SQLiteEditActivity",SQLiteEditActivity.class);

        EditText nameTextView = (EditText) solo.getView(R.id.sqlite_edit_edittext_name);
        solo.clearEditText(nameTextView);
        solo.enterText(nameTextView,"Test Name");

        EditText descriptionTextView = (EditText) solo.getView(R.id.sqlite_edit_edittext_description);
        solo.clearEditText(descriptionTextView);
        solo.enterText(descriptionTextView,"Test Description");

        solo.clickOnView(solo.getView(R.id.sqlite_edit_button_save));

        solo.waitForActivity(MainActivity.class.getName(),25);
        solo.assertCurrentActivity("Failure to start MainActivity",MainActivity.class);

        solo.clickOnView(solo.getView(R.id.main_button_sqlite_list));

        solo.waitForActivity(SQLiteListActivity.class.getName(),25);
        solo.assertCurrentActivity("Failure to start SQLiteListActivity",SQLiteListActivity.class);

        solo.clickInList(1);

        boolean nameTextFound = solo.searchText("Test Name");
        boolean descriptionTextFound = solo.searchText("Test Description");

        assertTrue(nameTextFound);
        assertTrue(descriptionTextFound);

        //Edit test
        nameTextView = (EditText) solo.getView(R.id.sqlite_edit_edittext_name);
        solo.clearEditText(nameTextView);
        solo.enterText(nameTextView,"Test Name (Edited)");

        descriptionTextView = (EditText) solo.getView(R.id.sqlite_edit_edittext_description);
        solo.clearEditText(descriptionTextView);
        solo.enterText(descriptionTextView,"Test Description (Edited)");

        solo.clickOnView(solo.getView(R.id.sqlite_edit_button_save));

        solo.waitForActivity(SQLiteListActivity.class.getName(),25);
        solo.assertCurrentActivity("Failure to start SQLiteListActivity",SQLiteListActivity.class);

        solo.clickInList(1);

        solo.clickOnView(solo.getView(R.id.sqlite_edit_button_delete));

        solo.finishOpenedActivities();
    }
}