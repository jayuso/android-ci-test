package com.android.ci.test.app;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.app.SherlockActivity;
import com.android.ci.test.app.dao.DataDAO;
import com.android.ci.test.app.util.Globals;

public class SQLiteListActivity extends SherlockActivity {

    ListView mListData = null;
    CursorAdapter mAdapter = null;
    ProgressBar mProgressBar =null;

    final String[] cols = {DataDAO.KEY_DATA_NAME,DataDAO.KEY_DATA_DESCRIPTION};
    final int[] views = {R.id.rowTextDataName,R.id.rowTextDataDescription};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressBar = (ProgressBar) findViewById(R.id.sqlitelist_activity_indicator);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mListData = (ListView) findViewById(R.id.sqlitelist_list_data_items);
        mListData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(getBaseContext(),SQLiteEditActivity.class);
                i.putExtra(Globals.EXTRA_DATA_ID,id);
                startActivity(i);
            }
        });

        refreshData();
    }

    private  void refreshData(){

        DataDAO dataDao = new DataDAO(this);
        Cursor c = dataDao.queryCursor();

        mAdapter = new SimpleCursorAdapter(this,
                R.layout.row_data,
                c,
                cols,
                views);

        mListData.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.sqlite_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
