package com.android.ci.test.app;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.EditText;

import com.jayway.android.robotium.solo.Solo;

/**
 * Created by javierayuso on 25/11/13.
 */
public class LaunchActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Solo solo;

    public LaunchActivityTest() {
        super(MainActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();

        solo = new Solo(getInstrumentation(),getActivity());
    }

    @MediumTest
    public void testLaunch() {

        EditText mainTextView = (EditText) solo.getView(R.id.main_edit_text);

        solo.clearEditText(mainTextView);
        solo.enterText(mainTextView,"Hello World!");

        solo.clickOnView(solo.getView(R.id.main_button_send));

        solo.waitForActivity(Activity2.class.getName(),25);
        solo.assertCurrentActivity("Failure to start Activity2",Activity2.class);

        solo.finishOpenedActivities();

    }

    @MediumTest
    public void testDragSlidingMenu() {

        solo.drag(10,100,400,400,1);
        solo.clickOnScreen(750,400);
        solo.finishOpenedActivities();

    }
}