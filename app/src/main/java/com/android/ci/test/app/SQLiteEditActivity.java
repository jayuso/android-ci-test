package com.android.ci.test.app;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.app.SherlockActivity;
import com.android.ci.test.app.dao.DataDAO;
import com.android.ci.test.app.data.Data;
import com.android.ci.test.app.util.Globals;

public class SQLiteEditActivity extends SherlockActivity {

    EditText nameEditText = null;
    EditText descriptionEditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite_edit);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final DataDAO dataDAO = new DataDAO(this.getBaseContext());
        final long dataId = this.getIntent().getLongExtra(Globals.EXTRA_DATA_ID,-1);

        if (dataId==-1) {
            ;
        } else {
            nameEditText = (EditText) findViewById(R.id.sqlite_edit_edittext_name);
            descriptionEditText = (EditText) findViewById(R.id.sqlite_edit_edittext_description);

            Data data = dataDAO.query(dataId);

            nameEditText.setText(data.getName());
            descriptionEditText.setText(data.getDescription());

        }

        Button saveButton = (Button) findViewById(R.id.sqlite_edit_button_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nameEditText = (EditText) findViewById(R.id.sqlite_edit_edittext_name);
                descriptionEditText = (EditText) findViewById(R.id.sqlite_edit_edittext_description);

                String name = nameEditText.getText().toString();
                String description = descriptionEditText.getText().toString();

                Data data = new Data(name,description);

                if (dataId==-1) {
                    dataDAO.insert(data);
                } else {
                    data.setId(dataId);
                    dataDAO.update(dataId, data);
                }

                finish();

            }
        });

        Button deleteButton = (Button) findViewById(R.id.sqlite_edit_button_delete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (dataId!=-1) {
                    dataDAO.delete(dataId);
                }
                finish();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.sqlite_edit, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
