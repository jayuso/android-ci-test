package com.android.ci.test.app.data;

/**
 * Created by javierayuso on 21/12/13.
 */
public class Data {

    long id;
    String name;
    String description;

    public Data() {

    }

    public Data(String name, String description) {
        this(-1,name,description);
    }

    public Data(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
